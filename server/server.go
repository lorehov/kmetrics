package server

import (
	"io"
	"fmt"
	"time"
	"strings"
	"net/http"
	"io/ioutil"
	"encoding/json"

	log "github.com/Sirupsen/logrus"
	"github.com/streadway/amqp"
	"code.google.com/p/go-uuid/uuid"
	"github.com/spf13/viper"

	"bitbucket.org/lorehov/kmetrics/worker"
	"bitbucket.org/lorehov/kmetrics/data"
)


type metricData struct {
	Username string	`json:"username"`
	Count int64		`json:"count"`
	Metric string	`json:"metric"`
}


type App struct {
	Channel *amqp.Channel
}


func onError(w http.ResponseWriter, code int, msg string, a ...interface{}) {
	message := fmt.Sprintf(msg, a)
	log.Error(message)
	http.Error(w, message, code)
}


func (a *App) AcceptMetric(w http.ResponseWriter, r *http.Request) {
	if strings.ToUpper(r.Method) != "POST" {
		onError(w, http.StatusMethodNotAllowed, "Method %s not allowed")
		return
	}

	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		onError(w, 422, "Request too big")
		return
	}

	log.Debugf("Metric %#v received", body)
	var uploadRequest metricData
	if err := json.Unmarshal(body, &uploadRequest); err != nil {
		onError(w, 422, "Failed to deserialize request: %s with error %s",
				body, err.Error())
		return
	}

	if uploadRequest.Username == "" || uploadRequest.Count == 0 || uploadRequest.Metric == "" {
		onError(w, 422, "Messing fields: %s", body)
		return
	}

	message, err := json.Marshal(worker.Message{
		Username:	uploadRequest.Username,
		Count:		uploadRequest.Count,
		Metric:		uploadRequest.Metric,
		Accepted:	time.Now().UTC(),
		Rid:		uuid.New()})

	if err != nil {
		onError(w, 500, "Cannt serialize message: %v", err)
		return
	}

	err = a.Channel.Publish(
		data.FrontendExchange.Name,
		data.FrontendExchange.DefaultRk,
		false,
		false,
		amqp.Publishing{
			ContentType:	"application/json",
			DeliveryMode:	amqp.Persistent,
			Body:			message})

	if err != nil {
		onError(w, 500, "Failed to publish to queue: %#v", err)
		return
	}

	w.WriteHeader(http.StatusCreated)
	fmt.Fprintf(w, "Ok")
}


func RunServer() {
	for {
		conn, err := amqp.Dial(viper.GetString("queue.connection"))
		if err != nil {
			log.Errorf("Failed to connect to RabbitMQ", err)
			time.Sleep(2)
			continue
		}
		defer conn.Close()

		data.DeclareTopology(conn)

		ch, err := conn.Channel()
		if err != nil {
			log.Errorf("Failed to open a channel", err)
			time.Sleep(2)
			continue
		}
		defer ch.Close()

		app := &App{ch}

		http.HandleFunc("/metrics", app.AcceptMetric)
		port := viper.GetString("web.port")
		log.Info("Kmetrics server started on port ", port)
		log.Panic(http.ListenAndServe(port, nil))
	}
}
