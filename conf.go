package main

import "github.com/spf13/viper"


func InitConfig(name string, test bool) {
	path := "./"
	if test {
		path = "../"
	}
	viper.SetConfigName(name)
	viper.AddConfigPath(path)
	viper.ReadInConfig()
}
