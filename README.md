How to launch
=============

With docker
-----------

Requirements:

  * docker
  * docker-compose
  
Run:
```bash
$ docker-compose up
```

When application starts following endpoints will be available:

 * metrics [POST]"http://[docker-host]:8080/metrics";
 * account-name worker metrics "http://[docker-host]:8081/debug/vars";
 * distinct-name worker metrics "http://[docker-host]:8082/debug/vars";
 * distinct-name-aggregator worker metrics "http://[docker-host]:8083/debug/vars";
 * hourly-log worker metrics "http://[docker-host]:8084/debug/vars";

Without docker
--------------

Requirements:

  * postgresql
  * redis
  * mongodb
  * rabbitmq
  * golang
  
When the requirements are met - you'll need to edit config.yml 
and launch application with following commands:

```bash

$ go get -v

$ go build

$ ./kmetrics --cmd=run-server &

$ ./kmetrics --cmd=run-worder --type=account-name &

$ ./kmetrics --cmd=run-worder --type=distinct-name &

$ ./kmetrics --cmd=run-worder --type=hourly-log &

$ ./kmetrics --cmd=siege &  # to test it
```

Architecture overview
=====================

Tasks distribution
------------------

Workers can be scaled horizontally as we use rabbitmq as a message broker 
with `direct` exchange for round-robin tasks distribution.
Also concurrency level per node can be adjusted as needed by changing `concurrency`
property in config.yml. It is possible because pattern `Worker/Job` was used
(see details of implementation in `kmetrics/worker/pool.go`).


Fault tolerance
---------------

If worker fails to process a task, task will be returned to queue and redeliver 
to another worker. When returning task to queue - `Redelivered` property will be checked 
and if task is already redelivered it will be sent to Dead Letter. 
To ensure that we don't lost messages in case of broker restart `persistent` queues 
and delivery mode were used (although it can impact system throughput). 

Recovery
--------

Worker can recover from any connection lost (database or rabbitmq). It is possible
due to some sort of supervising. Sadly implementation is not ideal, but as 
I shot on time - I decide to leave it as is. For details of implementation see
`kmetrics/worker/pool.go` (ErrChan on Pool / err on Executor) 
and `kmetrics/worker/common.go` - `RunWorker` function.
 

Metrics
-------

I decide not to implement complete backend for metrics collecting but instead 
implement small and lightweight solution based on expvars. See *Missing aspects*.

Missing aspects
---------------

Some important aspects weren't implemented as I'd running out of time. 

  * Backend for kmetrics's metrics. Currently I collect metrics inside of worker process
    and expose them via http. But this is not the best idea, because worker process can die
    and we lost all of these metrics. Here we have some options:
      * use an external solution like data dog;
      * instead of exposing metrics via http - save these in mongodb (or influx or whatever);
      * use lib such as `go-metrics` to send metrics in graphite (or analog);
  * Deadletters system. While deadletters exchange is in place, we still need 
    backend to store, view and replay that dead letters.
  
Calculating average in distributed environment
==============================================

In context of current task this is meaningless as we have one mongodb instance(cluster) 
and so we can just run `aggregate` queries against it.

But in real world - there are some possibilities though.

M/R solution
------------

If we don't need real-time average we can just run a cluster of kassandra/hbase/riak 
(these beasts already has XDC mechanisms out of the box) and run a M/R jobs on it.
Mongodb in some cases could be the good fit too, as it survives network partitioning pretty good.

Pros:

  * reliably
  * straightforward

Cons:

  * not real-time
  

Streaming solution
------------------

We can run two types of workers:

  * partinioned workers aggregating moving average (H);
  * worker(s) calculating current averages (A).

(H) are calculating moving averages in there partitions, and send
these averages to (A) from time to time. When (A) receives average, it uses
previous averages from other workers and calculates total average, 
based on these previous values + new one.

Example:
  
  * (A) has values from (H1), (H2), (H3)
  * (A) receives (H3n)
  * (A) calculates ((H1) + (H2) + (H3n))/3

Important nuance: every (H) must has it's own increasing counter so (A) can drop outdated messages if any.
 
Pros:

  * near real-time
  
Cons:

  * if some of (H) will die, we will see some average fluctuations;


Task description
================

You have a metric collector service. It handles all the requests with an HTTP
endpoint. But this endpoint doesn’t do anything more than dispatching these
requests to Rabbit MQ. All the requests will be processed by async workers. The
system has three async workers.

Write the async workers so that they match the following design descriptions
below:

First one (distinctName) collects daily occurrences of distinct events in Redis.
Metrics that are older than 30 days are merged into a monthly bucket, then
cleared.

Second one (hourlyLog) collects all items that occurred in the last hour into
MongoDB (Bonus: how would you calculate the average of incoming values in a
distributed environment?)

Third one (accountName) collects all the account names that sent metrics, with
their first occurrence datetime (UTC) into PostgreSQL.

Data sample:
{
  "username": "kodingbot",  // string
  "count": 12412414,    // int64
  "metric": "kite_call" // string
}

Important things to keep in mind while developing:

 * Logging: Use logging wisely. Debug mode should be verbose enough to understand
the program flow. Stay away from unnecessary usage of info, error, warning etc
levels of logs.

 * Metrics: How many requests were handled? How long did they take? What are our
average values? etc..

 * Distributed Architecture: All of the workers can be scaled horizontally.

 * Fault Tolerance: Crash of a worker should not cause any data loss. Retry
system for failed tasks should be in place.


Code should be written in Go (golang)
