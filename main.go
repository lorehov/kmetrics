package main

import (
	"fmt"
	"flag"
	"time"
	"math/rand"
	"encoding/json"
	"database/sql"

	_ "github.com/lib/pq"
	"github.com/spf13/viper"
	"github.com/parnurzeal/gorequest"
	log "github.com/Sirupsen/logrus"

	"bitbucket.org/lorehov/kmetrics/worker"
	"bitbucket.org/lorehov/kmetrics/server"
	"bitbucket.org/lorehov/kmetrics/data"
)


func initDb() {
	connectionString := data.GetPsqlConnectionString(
		viper.GetStringMap("worker-account-name.postgres"), "dbname")
	db, err := sql.Open("postgres", connectionString)
	defer db.Close()
	if err != nil {
		log.Panicf("Database opening error: %#v", err)
	}
	data.InitDb(db, true)
}


func beginSiege(w int) {
	rand.Seed(42)
	users := []string{
		"Bob", "Ivan", "Marly", "Jhon Doe", "Sebastjan", "Helen", "Helga"}
	metrics := []string{
		"length", "latency", "la", "cpu", "io", "ram"}
	host := fmt.Sprintf(
		"%s%s/metrics",
		viper.GetString("web.host"),
		viper.GetString("web.port"))

	for i := 0; i <=w; i++ {
		go func() {
			for {
				payload := map[string]interface{}{
					"username": users[rand.Intn(len(users))],
					"count": rand.Intn(100000),
					"metric": metrics[rand.Intn(len(metrics))]}
				mJson, _ := json.Marshal(payload)
				request := gorequest.New()
				_, _, errs := request.Post(host).
					Send(string(mJson)).
					End()

				if errs != nil {
					for err := range errs {
						log.Errorf("Error while sieging host %s: %#v", host, err)
					}
					panic("Something goes wrong!")
				}
				time.Sleep(1 * time.Second)
			}
		}()
	}
	for {}
}


func runWorker(workerType string) {
	switch workerType {
	case "account-name": worker.StartAccountNameWorker(
		viper.GetStringMap("worker-account-name"),
		viper.GetStringMap("queue"))
	case "hourly-log": worker.StartHourlyLogWorker(
		viper.GetStringMap("worker-hourly-log"),
		viper.GetStringMap("queue"))
	case "distinct-name":
		go worker.StartAggregatorWorker(
			viper.GetStringMap("worker-distinct-name"),
			viper.GetStringMap("queue"))
		worker.StartDistinctNameWorker(
			viper.GetStringMap("worker-distinct-name"),
			viper.GetStringMap("queue"))
	default:
		log.Fatalf("No worker %s.", workerType)
	}
}


func runServer() {
	log.ParseLevel(viper.GetString("web.log.level"))
	server.RunServer()
}


var cmd = flag.String("cmd", "", "command to execute: `initdb`, `siege`, `run-server`, `run-worker`")
var workerType = flag.String("type", "",
	"worker type to start, following types available: `account-name`, `distinct-name`, `hourly-log`")
var confName = flag.String("conf", "config", "name of config file to use")


func main() {
	flag.Parse()
	InitConfig(*confName, false)
	switch *cmd {
	case "initdb": initDb()
	case "siege": beginSiege(2)
	case "run-worker": runWorker(*workerType)
	case "run-server": runServer()
	default:
		fmt.Println("Please specify command.")
	}
}
