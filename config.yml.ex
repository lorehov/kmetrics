---
web:
  host: "http://192.168.59.103"  # for sieging only!
  port: ":8080"
  log:
    level: debug

worker-distinct-name:
  redis:
    addr: 192.168.59.103:6379
    password: ""
    db: 0
    db-test: 1
  concurrency:
    collector: 5
    aggregator: 2
  metrics:
    port: 8070
    port-aggregator: 8071
  log:
    level: debug

worker-hourly-log:
  mongodb:
    connection: 192.168.59.103:27019
    dbname: metrics
    dbname-test: metrics_test
  concurrency: 5
  metrics:
    port: 8082
  log:
    level: debug

worker-account-name:
  postgres:
    host: 192.168.59.103
    port: 5432
    user: kmetrics
    dbname: metrics
    dbname-test: metrics_test
  concurrency: 5
  metrics:
    port: 8093
  log:
    level: debug

queue:
  connection: "amqp://guest:guest@192.168.59.103:5672/"
