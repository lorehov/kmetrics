FROM golang:1.5.1

RUN mkdir -p /go/src/bitbucket.org/lorehov/kmetrics

ADD . /go/src/bitbucket.org/lorehov/kmetrics
WORKDIR /go/src/bitbucket.org/lorehov/kmetrics

RUN go get -d -v
RUN go install -v
RUN go build


