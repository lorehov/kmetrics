package data

import (
	"time"

	"gopkg.in/mgo.v2"
)


var metricsIndexes = []mgo.Index{
	mgo.Index{
		Key: []string{"accepted"},
		Background: true,
		ExpireAfter: 60 * time.Minute,
	},
}


func EnsureIndexes(db *mgo.Database) {
	metrics := db.C("metrics")
	for _, idx := range metricsIndexes {
		metrics.EnsureIndex(idx)
	}
}
