package data

import (
	"github.com/streadway/amqp"
	log "github.com/Sirupsen/logrus"
)


type Exchange struct {
	Name string
	Type string
	Durable bool
	DefaultRk string
}

var FrontendExchange = Exchange{
	Name: "frontend",
	Type: "fanout",
	Durable: true,
	DefaultRk: "metric"}

var AccountNameExchange = Exchange{
	Name: "accountName",
	Type: "direct",
	Durable: true,
	DefaultRk: "metric"}

var DistinctNameExchange = Exchange{
	Name: "distinctName",
	Type: "direct",
	Durable: true,
	DefaultRk: "metric"}

var DistinctNameAggregateExchange = Exchange{
	Name: "distinctNameAggregate",
	Type: "direct",
	Durable: true,
	DefaultRk: "metric"}

var HourlyLogExchange = Exchange{
	Name: "distinctName",
	Type: "direct",
	Durable: true,
	DefaultRk: "metric"}

var DeadLettersExchange = Exchange{
	Name: "deadLettes",
	Type: "direct",
	Durable: true,
	DefaultRk: ""}


func declareExchange(ex Exchange, ch *amqp.Channel) {
	err := ch.ExchangeDeclare(
		ex.Name, ex.Type, ex.Durable, false, false, false, nil)
	if err != nil {
		log.Panicf("Failed to declare %s exchange with error: %q", ex.Name, err)
	}
}

func DeclareTopology(conn *amqp.Connection) {
	ch, err := conn.Channel()
	if err != nil {
		log.Fatalf("Failed to open a channel: %v", err)
	}
	defer ch.Close()

	declareExchange(FrontendExchange, ch)
	declareExchange(DeadLettersExchange, ch)
	declareExchange(DistinctNameAggregateExchange, ch)

	exchangesToBind := []Exchange{
		AccountNameExchange, DistinctNameExchange, HourlyLogExchange}

	for _, ex := range exchangesToBind {
		declareExchange(ex, ch)

		err = ch.ExchangeBind(
			ex.Name,
			FrontendExchange.DefaultRk,
			FrontendExchange.Name,
			false, nil)
		if err != nil {
			log.Fatalf(
				"Failed to bind %q exchange to frontend exchange: %v", ex, err)
		}
	}
}
