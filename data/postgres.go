package data

import (
	"fmt"
	"database/sql"

	log "github.com/Sirupsen/logrus"
)

func GetPsqlConnectionString(params map[string]interface{}, dbnameToUse string) string {
	return fmt.Sprintf("host=%s port=%v user=%s dbname=%s sslmode=disable",
		params["host"], params["port"], params["user"], params[dbnameToUse])
}

func InitDb(db *sql.DB, recreate bool) {
	initializeSequence := []string{
		`CREATE TABLE accounts(
		 id serial,
		 account varchar(100),
		 created timestamp with time zone,
		 constraint id_pk primary key (id));`,

		"CREATE UNIQUE INDEX account_idx ON accounts (account);",
		`CREATE OR REPLACE FUNCTION ensureCreated(name varchar(100), c timestamp) RETURNS int
		AS
			$$
			BEGIN
				IF EXISTS (SELECT 1 FROM accounts ac WHERE ac.account = name AND ac.created < c) THEN
					RETURN 1;
				END IF;
				INSERT INTO accounts(account, created) VALUES(name, c);
				RETURN 1;
			EXCEPTION WHEN unique_violation THEN
				UPDATE accounts SET created = c WHERE account = name AND created > c;
				RETURN 1;
			END;
		$$ LANGUAGE plpgsql`}

	if recreate {
		initializeSequence = append(
			[]string{
				"DROP TABLE IF EXISTS accounts CASCADE;",
				"DROP FUNCTION IF EXISTS ensureCreated(name varchar(100), c timestamp);",
			}, initializeSequence...)

	}

	for _, query := range initializeSequence {
		_, err := db.Exec(query)

		if err != nil {
			log.Panicf("Database init error -->%v\n", err)
		}
	}
	log.Info("Database rebuilded successfully")
}
