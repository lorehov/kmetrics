package worker_test

import (
	"fmt"
	"time"
	"errors"
	"strings"
	"testing"
	"encoding/json"

	"gopkg.in/redis.v3"
	"github.com/spf13/viper"
	"github.com/streadway/amqp"
	log "github.com/Sirupsen/logrus"

	k "bitbucket.org/lorehov/kmetrics"
	"bitbucket.org/lorehov/kmetrics/worker"
)


type testCaseDDaily struct {
	Descr string
	Msgs []worker.Message
	Buckets map[string][]string  // we check only request ids
	ExistingBuckets []string
}


func TestDailyBuckets(t *testing.T) {
	client := getRedisClient()

	testCases := []testCaseDDaily{
		testCaseDDaily{
			"Should create bucket if not exist",
			[]worker.Message{
				worker.Message{
					Metric: "la",
					Count: 10,
					Accepted: time.Date(2015, 1, 1, 21, 0, 0, 0, time.UTC),
					Rid: "1"},
				worker.Message{
					Metric: "la",
					Count: 15,
					Accepted: time.Date(2015, 1, 1, 23, 0, 0, 0, time.UTC),
					Rid: "2"},
				worker.Message{
					Metric: "la",
					Count: 9,
					Accepted: time.Date(2015, 1, 1, 20, 0, 0, 0, time.UTC),
					Rid: "3"},
				worker.Message{
					Metric: "vss",
					Count: 100,
					Accepted: time.Date(2015, 1, 1, 20, 0, 0, 0, time.UTC),
					Rid: "0"},
				worker.Message{
					Metric: "la",
					Count: 10,
					Accepted: time.Date(2015, 1, 2, 0, 0, 0, 0, time.UTC),
					Rid: "10"},
			},
			map[string][]string{
				"la:2015-01-01": []string{"1", "2", "3"},
				"vss:2015-01-01": []string{"0"},
				"la:2015-01-02": []string{"10"}},
			[]string{}},
		testCaseDDaily{
			"Should add to existing buckets",
			[]worker.Message{
				worker.Message{
					Metric: "la",
					Count: 0,
					Accepted: time.Date(2015, 1, 1, 1, 1, 1, 1, time.UTC),
					Rid: "2"},
				worker.Message{
					Metric: "la",
					Count: 9,
					Accepted: time.Date(2015, 1, 1, 2, 2, 2, 2, time.UTC),
					Rid: "3"},
				worker.Message{
					Metric: "vss",
					Count: 100,
					Accepted: time.Date(2015, 1, 1, 20, 0, 0, 0, time.UTC),
					Rid: "0"},
				worker.Message{
					Metric: "vss",
					Count: 10,
					Accepted: time.Date(2015, 1, 2, 0, 0, 0, 0, time.UTC),
					Rid: "10"},
			},
			map[string][]string{
				"la:2015-01-01": []string{"2", "3"},
				"vss:2015-01-01": []string{"0"},
				"vss:2015-01-02": []string{"10"},
			},
			[]string{"la:2015-01-01", "vss:2015-01-01", "vss:2015-01-02"},
		}}

	for _, tc := range testCases {
		client.FlushDb()
		for i, eb := range tc.ExistingBuckets {
			splitted := strings.Split(eb, ":")
			metric, day := splitted[0], splitted[1]
			dailyKey := metric + ":daily"
			bucketKey := metric + ":" + day
			client.ZAdd(dailyKey, redis.Z{Score: float64(i), Member: bucketKey})
			client.RPush(bucketKey, "")
		}

		for _, msg := range tc.Msgs {
			body, _ := json.Marshal(msg)
			job := worker.DistinctNameJob{
				Db: client,
				Delivery: amqp.Delivery{
					Body: body},
				Ack: &MockAcknowledger{}}
			job.Run()
		}

		for key, rids := range tc.Buckets {
			setOk, bucketOk := isDailyBucketCorrect(key, rids, client)
			if !setOk || !bucketOk {
				t.Errorf(
					"Test '%s' failed, setOk = %#v bucketOk = %#v for key %s",
					tc.Descr,
					setOk,
					bucketOk,
					key)
			}
		}
	}
}


type testCaseAggregate struct {
	Descr string
	Daily map[string][]worker.Message
	Monthly map[string][]string  // we check only request ids
}


func TestAggregateMonthly(t *testing.T) {
	client := getRedisClient()

	current := UTC()
	toAggregate := current.Add(-24 * time.Hour).Add(-worker.HOURS_TO_AGGREGATE * time.Hour)
	metricKeys := []string{"la", "cpu"}
	testCases := []testCaseAggregate{
		testCaseAggregate{
			Descr: "Should not aggregate actual buckets",
			Daily: map[string][]worker.Message {
				_dailyKey("la", current): []worker.Message{
					worker.Message{Rid: "1", Accepted: current},
					worker.Message{Rid: "2", Accepted: current},
				},
				_dailyKey("cpu", current): []worker.Message{
					worker.Message{Rid: "3", Accepted: current},
					worker.Message{Rid: "4", Accepted: current},
				},
			},
			Monthly: map[string][]string {},
		},

		testCaseAggregate{
			Descr: "Should aggregate buckets older then 30 days",
			Daily: map[string][]worker.Message {
				_dailyKey("la", current): []worker.Message{
					worker.Message{Rid: "0", Accepted: current},
				},
				_dailyKey("la", toAggregate): []worker.Message{
					worker.Message{Rid: "1", Accepted: toAggregate},
					worker.Message{Rid: "2", Accepted: toAggregate},
				},
				_dailyKey("cpu", toAggregate): []worker.Message{
					worker.Message{Rid: "3", Accepted: toAggregate},
					worker.Message{Rid: "4", Accepted: toAggregate},
				},
			},
			Monthly: map[string][]string {
				"la:monthly": []string{"1", "2"},
				"cpu:monthly": []string{"3", "4"},
			},
		},

	}
	for _, tc := range testCases {
		client.FlushDb()

		for k, msgs := range tc.Daily {
			saveDailyBucket(k, msgs, client)
		}

		for _, mk := range metricKeys {
			client.SAdd("metrics", mk)
			body, _ := json.Marshal(worker.AggregateMsg{Metric: mk})
			job := worker.AggregatorJob{
				Db: client,
				Delivery: amqp.Delivery{Body: body},
				Ack: &MockAcknowledger{}}
			job.Run()
		}

		for k, rids := range tc.Monthly {
			if err := isMonthlyBucketCorrect(k, rids, client); err != nil {
				t.Errorf("Test '%s' failed with error %#v", tc.Descr, err)
			}
		}
	}
}


type testCaseRemove struct {
	Descr string
	Monthly map[string][]worker.Message
	MonthlyExpected map[string][]string  // we check only request ids
}


func TestRemoveOutdated(t *testing.T) {
	client := getRedisClient()

	current := UTC()
	toAggregate := current.Add(-24 * time.Hour).Add(-worker.HOURS_TO_AGGREGATE * time.Hour)
	toRemove := current.Add(-24 * time.Hour).Add(-worker.HOURS_TO_REMOVE * time.Hour)
	metricKeys := []string{"la", "cpu"}
	testCases := []testCaseRemove{
		testCaseRemove{
			Descr: "Should not remove if not too old",
			Monthly: map[string][]worker.Message{
				"la:monthly": []worker.Message{
					worker.Message{Rid: "1", Accepted: toAggregate},
					worker.Message{Rid: "2", Accepted: toAggregate},
				},
				"cpu:monthly": []worker.Message{
					worker.Message{Rid: "3", Accepted: toAggregate},
					worker.Message{Rid: "4", Accepted: toAggregate},
					worker.Message{Rid: "5", Accepted: toAggregate},
				},
			},
			MonthlyExpected: map[string][]string{
				"la:monthly": []string{"1", "2"},
				"cpu:monthly": []string{"3", "4", "5"},
			},
		},

		testCaseRemove{
			Descr: "Should remove too old entries",
			Monthly: map[string][]worker.Message{
				"la:monthly": []worker.Message{
					worker.Message{Rid: "1", Accepted: toAggregate},
					worker.Message{Rid: "2", Accepted: toRemove},
					worker.Message{Rid: "3", Accepted: toRemove},
					worker.Message{Rid: "4", Accepted: toRemove},
				},
			},
			MonthlyExpected: map[string][]string{
				"la:monthly": []string{"1"},
			},
		},

	}

	for _, tc := range testCases {
		client.FlushDb()

		for k, msgs := range tc.Monthly {
			saveMonthlyBucket(k, msgs, client)
		}

		for _, mk := range metricKeys {
			client.SAdd("metrics", mk)
			body, _ := json.Marshal(worker.AggregateMsg{Metric: mk})
			job := worker.AggregatorJob{
				Db: client,
				Delivery: amqp.Delivery{Body: body},
				Ack: &MockAcknowledger{}}
			job.Run()
		}

		for k, rids := range tc.MonthlyExpected {
			if err := isMonthlyBucketCorrect(k, rids, client); err != nil {
				t.Errorf("Test '%s' failed with error %#v", tc.Descr, err)
			}
		}
	}
}


func getRedisClient() *redis.Client {
	k.InitConfig("config", true)
	client := redis.NewClient(&redis.Options{
		Addr:		viper.GetString("worker-distinct-name.redis.addr"),
		Password:	viper.GetString("worker-distinct-name.redis.password"),
		DB:			int64(viper.GetInt("worker-distinct-name.redis.db-test")),
	})

	_, err := client.Ping().Result()
	if err != nil {
		log.Panicf("Couldn't connect to redis: %#v", err)
	}
	return client
}


func isDailyBucketCorrect(key string, rids []string, client *redis.Client) (bool, bool) {
	splitted := strings.Split(key, ":")
	metric, _ := splitted[0], splitted[1]
	dailyKey := metric + ":daily"

	setOk, bucketOk := true, true
	result, _ := client.ZScore(dailyKey, key).Result()
	if result == 0 {
		setOk = false
	}

	len := client.LLen(key).Val()
	skipped := false
	for i, val := range client.LRange(key, 0, len).Val() {
		if val == "" {
			skipped = true
			continue
		}

		var msg worker.Message
		err := json.Unmarshal([]byte(val), &msg)
		if err != nil {
			log.Errorf("Can't deserialize message %#v: %#v", val, err)
		}

		if skipped {
			i = i - 1
		}

		if msg.Rid != rids[i] {
			bucketOk = false
		}
	}
	return setOk, bucketOk
}


func isMonthlyBucketCorrect(k string, rids []string, client *redis.Client) error {
	count, err := client.ZCount(k, "0", "99999999").Result()
	if err != nil {
		return err
	}

	if len(rids) != int(count) {
		return createErr("Size of set %#v but must be %#v", count, len(rids))
	}

	ridsSet := make(map[string]bool)
	for _, r := range rids {
		ridsSet[r] = true
	}

	res, err := client.ZRangeByScore(
		k, redis.ZRangeByScore{Min: "0", Max: "99999999"}).Result()
	if err != nil {
		return createErr("Can't retrive metrics to aggregate: %#v", err)
	}

	for _, data := range res {
		var msg worker.Message
		json.Unmarshal([]byte(data), &msg)
		if _, ok := ridsSet[msg.Rid]; !ok {
			return createErr("Wrong rid %#v found", msg.Rid)
		}
	}
	return nil
}


func saveDailyBucket(k string, msgs []worker.Message, client *redis.Client) {
	for _, msg := range msgs {
		score := worker.CalculateScore(msg)
		splitted := strings.Split(k, ":")
		metric := splitted[0]
		err := client.ZAdd(metric + ":daily", redis.Z{Score: score, Member: k}).Err()
		if err != nil {
			log.Panicf("Can't add key to daily buckets set: %#v", err)
		}

		body, _ := json.Marshal(msg)
		if err := client.RPush(k, string(body)).Err(); err != nil {
			log.Panic("Can't add element to daily bucket: %#v", err)
		}
	}
}


func saveMonthlyBucket(k string, msgs []worker.Message, client *redis.Client) {
	for _, msg := range msgs {
		body, _ := json.Marshal(msg)
		s := worker.CalculateScore(msg)
		if err := client.ZAdd(k, redis.Z{Score: s, Member: body}).Err();
			err != nil {
			log.Panic("Can't add element to monthly bucket: %#v", err)
		}
	}
}


func _dailyKey(m string, d time.Time) string {
	return fmt.Sprintf("%s:%s", m, d.Format("2006-01-02"))
}


func createErr(fmtStr string, args ...interface{}) error {
	str := fmt.Sprintf(fmtStr, args...)
	return errors.New(str)
}

