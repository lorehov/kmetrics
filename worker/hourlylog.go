package worker

import (
	"time"
	"encoding/json"

	"gopkg.in/mgo.v2"
	"github.com/streadway/amqp"
	log "github.com/Sirupsen/logrus"

	"bitbucket.org/lorehov/kmetrics/data"
)


type HourlyLogWorker struct {
	Db *mgo.Database
	name string
	metric *Metric
}


func (a *HourlyLogWorker) Consume(pool *Pool, messages <-chan amqp.Delivery) {
	for d := range messages {
		job := JobWithMetrics{
			job: HourlyLogJob{
				Delivery: d,
				Ack: &AmqpAcknowledger{},
				Db: a.Db},
			metric: a.metric}

		pool.JobQueue <- job
	}
}


func (a *HourlyLogWorker) Name() string {
	return a.name
}


func StartHourlyLogWorker(wconf Options, qconf Options) {
	w := 0
	dbconf := wconf.GetMap("mongodb")
	c := wconf.GetInt("concurrency")
	log.ParseLevel(wconf.GetMap("log").GetString("level"))

	for {
		Wait(w)

		session, err := mgo.Dial(dbconf.GetString("connection"))
		defer session.Close()
		if err != nil {
			log.Errorf("Couldn't connect to mongodb: %#v", err)
			continue
		}

		db := session.DB(wconf.GetString("dbname"))
		data.EnsureIndexes(db)

		metric := StartMetricsEndpoint(
			wconf.GetMap("metrics").GetString("port"), "hourlyLog")
		worker := &HourlyLogWorker{
			Db: db,
			name: "hourlyLog",
			metric: metric}
		err = RunWorker(qconf, data.HourlyLogExchange, c, worker)
		if err != nil {
			log.Error(err.Error())
			w = 1
		}
	}
}


type HourlyLogJob struct {
	Delivery amqp.Delivery
	Db *mgo.Database
	Ack Acknowledger
}


func (j HourlyLogJob) Run() error {
	var msg Message
	if err := json.Unmarshal(j.Delivery.Body, &msg); err != nil {
		log.Errorf("Can't umnarshall message %q", j.Delivery.Body)
		j.Ack.Nack(j.Delivery, false)
		return nil
	}

	log.Debugf("Message received %q", msg)
	if msg.Accepted.Before(time.Now().UTC().Add(-1 * time.Hour)) {
		return nil
	}

	err := j.Db.C("metrics").Insert(msg)
	if err != nil {
		j.Ack.Nack(j.Delivery, true)
		return FormatError("Failed to save msg %#v with error", msg, err)
	}

	j.Ack.Ack(j.Delivery)
	return nil
}
