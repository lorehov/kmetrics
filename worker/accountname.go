package worker

import (
	"encoding/json"
	"database/sql"

	"github.com/streadway/amqp"
	log "github.com/Sirupsen/logrus"
	_ "github.com/lib/pq"

	"bitbucket.org/lorehov/kmetrics/data"
)


type AccountNameWorker struct {
	Db *sql.DB
	name string
	metric *Metric
}


func (a *AccountNameWorker) Consume(pool *Pool, messages <-chan amqp.Delivery) {
	for d := range messages {
		job := JobWithMetrics{
			job: AccountNameJob{
				Delivery: d,
				Ack: &AmqpAcknowledger{},
				Db: a.Db},
			metric: a.metric}
		pool.JobQueue <- job
	}
}


func (a *AccountNameWorker) Name() string {
	return a.name
}


func StartAccountNameWorker(wconf Options, qconf Options) {
	w := 0
	dbconf := wconf.GetMap("postgres")
	c := wconf.GetInt("concurrency")
	log.ParseLevel(wconf.GetMap("log").GetString("level"))
	for {
		Wait(w)
		connString := data.GetPsqlConnectionString(dbconf, "dbname")
		db, err := sql.Open("postgres", connString)
		defer db.Close()

		if err != nil {
			log.Errorf("Database opening error: %#v", err)
			continue
		}

		metric := StartMetricsEndpoint(
			wconf.GetMap("metrics").GetString("port"), "accountName")
		worker := &AccountNameWorker{
			Db: db,
			name: "accountName",
			metric: metric}

		err = RunWorker(qconf, data.AccountNameExchange, c, worker)
		if err != nil {
			log.Error(err.Error())
			w = 1
		}
	}
}


type AccountNameJob struct {
	Delivery amqp.Delivery
	Db *sql.DB
	Ack Acknowledger
}


func (j AccountNameJob) Run() error {
	var msg Message
	if err := json.Unmarshal(j.Delivery.Body, &msg); err != nil {
		log.Errorf("Can't umnarshall message %#v", j.Delivery.Body)
		j.Ack.Nack(j.Delivery, false)
		return nil
	}

	log.Debugf("Message %+v received", msg)
	/*
	May be not the best solution but in context of this task it seems to be the best choice.
	Alternatives:
		* Use postgres >=9.5 with ON CONFLICT feature;
		* Make 3 consequent queries (not so bad if we think about it);
	 */
	_, err := j.Db.Query(
		"select ensureCreated($1, $2)", msg.Username, msg.Accepted)

	if err != nil {
		j.Ack.Nack(j.Delivery, true)
		return FormatError(
			"Insert/update was failed with error: %+v for message %+v", err, msg)
	}

	j.Ack.Ack(j.Delivery)
	return nil
}

