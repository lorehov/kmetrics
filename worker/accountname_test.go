package worker_test

import (
	"time"
	"testing"
	"database/sql"
	"encoding/json"

	_ "github.com/lib/pq"
	"github.com/spf13/viper"
	"github.com/streadway/amqp"
	log "github.com/Sirupsen/logrus"

	"bitbucket.org/lorehov/kmetrics/worker"
	"bitbucket.org/lorehov/kmetrics/data"
	k "bitbucket.org/lorehov/kmetrics"
)


func newJob(db *sql.DB, msg worker.Message) (worker.AccountNameJob, *MockAcknowledger) {
	acknowledger := &MockAcknowledger{}
	body, _ := json.Marshal(msg)
	return worker.AccountNameJob{
		Db: db,
		Ack: acknowledger,
		Delivery: amqp.Delivery{
			Body: body,
		}}, acknowledger
}


func getLast(db *sql.DB, username string) time.Time {
	var created time.Time
	err := db.QueryRow("SELECT created FROM accounts WHERE account = $1", username).
		Scan(&created)
	if err != nil {
		log.Printf(err.Error())
	}
	return created
}


type testCaseA struct {
	Created time.Time
	Result time.Time
	TimeBefore time.Time
	Acked bool
}


func TestAccountNameWorker(t *testing.T) {
	k.InitConfig("config", true)

	db, err := sql.Open("postgres", data.GetPsqlConnectionString(
		viper.GetStringMap("worker-account-name.postgres"), "dbname-test"))
	defer db.Close()
	if err != nil {
		log.Fatalf("Cannot connect to db: %q", err)
	}

	data.InitDb(db, true)

	testCases := []testCaseA{
		testCaseA{
			time.Now().UTC(), time.Now().UTC(), *new(time.Time), true},
		testCaseA{
			time.Now().UTC(), time.Now().AddDate(0, 0, -1).UTC(),
			time.Now().AddDate(0, 0, -1).UTC(), true},
		testCaseA{
			time.Now().UTC(), time.Now().UTC(),
			time.Now().AddDate(0, 0, 1).UTC(), true},
	}

	for _, tc := range testCases {
		db.Exec("DELETE FROM accounts;")
		if tc.TimeBefore != *new(time.Time) {
			db.Exec("INSERT INTO accounts (account, created) VALUES ('test1', $1)", tc.TimeBefore)
		}

		msg := worker.Message{
			Username: "test1", Count: 1, Metric: "la", Accepted: tc.Created}
		job, ack := newJob(db, msg)
		job.Run()
		if ack.Acked != tc.Acked {
			t.Errorf(
				"Job Ack should be %#v, but %#v found for tc %#v",
				tc.Acked, ack.Acked, tc)
		}

		created := getLast(db, "test1")
		if !CompareDt(tc.Result, created) {
			t.Errorf(
				"Expecting time %#v but %#v got for tc %#v",
				tc.Result, created, tc)
		}
	}

	acknowledger := &MockAcknowledger{}
	body := []byte("wrong body")
	job := worker.AccountNameJob{
		Db: db,
		Ack: acknowledger,
		Delivery: amqp.Delivery{
			Body: body,
		}}
	job.Run()

	if !acknowledger.Nacked {
		t.Errorf("Test case should fail, because of wrong message body!")
	}
}
