package worker

import (
	"net"
	"time"
	"sync"
	"expvar"
	"net/http"
	"sync/atomic"

	"github.com/paulbellamy/ratecounter"
	log "github.com/Sirupsen/logrus"
	"strconv"
)

type Metric struct {
	rate *ratecounter.RateCounter
	counter *ratecounter.Counter
	durations int64

	lock *sync.Mutex
	metrics *expvar.Map
}


type IntVar struct {
	value int64
}


func (v IntVar) String() string {
	return strconv.FormatInt(v.value, 10)
}


func NewIntVar(v int64) IntVar {
	return IntVar{v}
}


type JobWithMetrics struct {
	metric *Metric
	job Job
}


func (j JobWithMetrics) Run() error {
	defer j.metric.Update(time.Now())
	return j.job.Run()
}

func (m *Metric) Update(started time.Time) {
	duration := time.Now().Sub(started)
	m.counter.Incr(1)
	m.rate.Incr(1)
	atomic.AddInt64(&m.durations, int64(duration))

	m.lock.Lock()
	defer m.lock.Unlock()

	m.metrics.Set("processed", NewIntVar(m.counter.Value()))
	m.metrics.Set("rate", NewIntVar(m.rate.Rate()))
	m.metrics.Set("average", NewIntVar(m.durations/m.counter.Value()))
}


func StartMetricsEndpoint(port string, name string) *Metric {
	host := "0.0.0.0:" + port
	sock, err := net.Listen("tcp", host)
	if err != nil {
	  log.Debug("Can't listen %s as %#v", host, err)
	}

	metrics := expvar.NewMap(name)
	go func() {
	  log.Infof("Metrics for %s now available at port %s", name, port)
	  http.Serve(sock, nil)
	}()
	counter := ratecounter.Counter(0)
	return &Metric{
		rate: ratecounter.NewRateCounter(time.Minute),
		counter: &counter,
		durations: 0,
		lock: &sync.Mutex{},
		metrics: metrics}
}
