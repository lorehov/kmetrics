/*
	Data structure:
	Lock:
		"lastExecuted": "some-date"
		"lockedBy": "some-app-id"

	metrics set:
		"metrics": Set{"metricName1", "metricName2"}

	daily:
		"metricName:daily": ZSet{"metricName:2015-01-01"...}
		"metricName:2015-01-01": List{Msg{}...}

	monthly:
		"metricName:monthly": ZSet{Msg{}...}
 */
package worker

import (
	"time"
	"strconv"
	"encoding/json"

	"gopkg.in/redis.v3"
	"github.com/streadway/amqp"
	log "github.com/Sirupsen/logrus"

	"bitbucket.org/lorehov/kmetrics/data"
	"code.google.com/p/go-uuid/uuid"
)


const HOURS_TO_AGGREGATE = 24 * 30
const HOURS_TO_REMOVE = 24 * 60


type DistinctNameWorker struct {
	Db *redis.Client
	name string
	metric *Metric

}


func (a *DistinctNameWorker) Consume(pool *Pool, messages <-chan amqp.Delivery) {
	for d := range messages {
		job := JobWithMetrics{
			job: DistinctNameJob{
				Delivery: d,
				Ack: &AmqpAcknowledger{},
				Db: a.Db},
			metric: a.metric}

		pool.JobQueue <- job
	}
}


func (a *DistinctNameWorker) Name() string {
	return a.name
}


func StartDistinctNameWorker(wconf Options, qconf Options) {
	dbconf := wconf.GetMap("redis")
	c := wconf.GetMap("concurrency").GetInt("collector")
	log.ParseLevel(wconf.GetMap("log").GetString("level"))
	w := 0
	for {
		Wait(w)
		client := redis.NewClient(&redis.Options{
			Addr:        dbconf.GetString("addr"),
			Password:    dbconf.GetString("pwd"),
			DB:          dbconf.GetInt64("db"),
		})

		_, err := client.Ping().Result()
		if err != nil {
			log.Errorf("Couldn't connect to redis: %#v", err)
			continue
		}

		metric := StartMetricsEndpoint(
			wconf.GetMap("metrics").GetString("port"), "distinctName")
		worker := &DistinctNameWorker{
			Db: client,
			name: "distinctName",
			metric: metric}
		err = RunWorker(qconf, data.DistinctNameExchange, c, worker)
		if err != nil {
			log.Error(err.Error())
			w = 1
		}
	}
}


type DistinctNameJob struct {
	Delivery amqp.Delivery
	Db *redis.Client
	Ack Acknowledger
}


func (j DistinctNameJob) Run() error {
	var msg Message
	if err := json.Unmarshal(j.Delivery.Body, &msg); err != nil {
		log.Errorf("Can't umnarshall message %q", j.Delivery.Body)
		j.Ack.Nack(j.Delivery, false)
		return nil
	}

	log.Debugf("Message received %q", msg)

	if !j.Db.SIsMember("metrics", msg.Metric).Val() {
		_, err := j.Db.SAdd("metrics", msg.Metric).Result()
		if err != nil {
			j.Ack.Nack(j.Delivery, true)
			return FormatError("Add %s to metrics error: %#v", msg.Metric, err)
		}
	}

	rawData := string(j.Delivery.Body)
	dailyKey := msg.Metric + ":daily"
	bucketKey := msg.Metric + ":" + msg.Accepted.Format("2006-01-02")
	res, _ := j.Db.ZScore(dailyKey, bucketKey).Result()

	if res == 0 {
		score := CalculateScore(msg)
		err := j.Db.ZAdd(dailyKey, redis.Z{Score: score, Member: bucketKey}).Err()
		if err != nil {
			j.Ack.Nack(j.Delivery, true)
			return FormatError("Adding daily bucket to set error: %#v", err.Error())
		}
	}

	_, err := j.Db.RPush(bucketKey, rawData).Result()
	if err != nil {
		j.Ack.Nack(j.Delivery, true)
		return FormatError("Adding event to daily bucket error: %#v", err.Error())
	}

	j.Ack.Ack(j.Delivery)
	return nil
}


type AggregatorWorker struct {
	Db *redis.Client
	name string
	metric *Metric
}


func (a *AggregatorWorker) Consume(pool *Pool, messages <-chan amqp.Delivery) {
	for d := range messages {
		job := JobWithMetrics{
			job: AggregatorJob{
				Delivery: d,
				Ack: &AmqpAcknowledger{},
				Db: a.Db},
			metric: a.metric}

		pool.JobQueue <- job
	}
}


func (a *AggregatorWorker) Name() string {
	return a.name
}


func StartAggregatorWorker(wconf Options, qconf Options) {
	dbconf := wconf.GetMap("redis")
	c := wconf.GetMap("concurrency").GetInt("aggregator")
	log.ParseLevel(wconf.GetMap("log").GetString("level"))

	w := 0
	for {
		Wait(w)

		client := redis.NewClient(&redis.Options{
			Addr:        dbconf.GetString("addr"),
			Password:    dbconf.GetString("pwd"),
			DB:          dbconf.GetInt64("db"),
		})

		_, err := client.Ping().Result()
		if err != nil {
			log.Errorf("Couldn't connect to redis: %#v", err)
			continue
		}

		metric := StartMetricsEndpoint(
			wconf.GetMap("metrics").GetString("port-aggregator"),
			"distinctNameAggregator")
		worker := &AggregatorWorker{
			Db: client,
			name: "distinctNameAggregator",
			metric: metric}
		ticker := time.NewTicker(24 * time.Hour)

		go loopScheduleAggregation(client, ticker, qconf)

		err = RunWorker(qconf, data.DistinctNameAggregateExchange, c, worker)
		if err != nil {
			w = 1
			log.Error(err.Error())
		}
		ticker.Stop()
	}
}


type AggregateMsg struct {
	Metric string `json:"metric"`
}


type AggregatorJob struct {
	Delivery amqp.Delivery
	Db *redis.Client
	Ack Acknowledger
}


func (j AggregatorJob) Run() error {
	var msg AggregateMsg
	if err := json.Unmarshal(j.Delivery.Body, &msg); err != nil {
		log.Errorf("Can't umnarshall message %q", j.Delivery.Body)
		j.Ack.Nack(j.Delivery, false)
		return nil
	}

	log.Debugf("Message received %q", msg)

	dailyKey := msg.Metric + ":daily"
	monthlyKey := msg.Metric + ":monthly"
	aggregateDate := time.Now().UTC().Add(-HOURS_TO_AGGREGATE * time.Hour)
	aggregateScore := aggregateDate.Format("20060102")

	res, err := j.Db.ZRangeByScore(
		dailyKey, redis.ZRangeByScore{Min: "0", Max: aggregateScore}).Result()
	if err != nil {
		j.Ack.Nack(j.Delivery, true)
		return FormatError("Can't retrive metrics to aggregate: %#v", err)
	}

	// aggregate daily logs
	for _, bucketKey := range res {
		len, err := j.Db.LLen(bucketKey).Result()
		if err != nil {
			j.Ack.Nack(j.Delivery, true)
			return FormatError(
				"Can't get len from list %s: %#v", bucketKey, err)
		}

		res, err := j.Db.LRange(bucketKey, 0, len).Result()
		if err != nil {
			j.Ack.Nack(j.Delivery, true)
			return FormatError(
				"Failed to iterate over values in daily bucker %s: %#v",
				bucketKey, err)
		}

		for _, val := range res  {
			var msg Message
			if err := json.Unmarshal([]byte(val), &msg); err != nil {
				// very strange situation, but anyway :-/
				log.Errorf("Can't umnarshall message %q", val)
				return nil
			}
			score := CalculateScore(msg)
			j.Db.ZAdd(monthlyKey, redis.Z{Score: score, Member: val})
		}

		_, err = j.Db.Del(bucketKey).Result()
		if err != nil {
			j.Ack.Nack(j.Delivery, true)
			return FormatError("Failed to delete outdated keys")
		}
	}

	// clean up monthly logs
	outdatedDate := time.Now().UTC().Add(-HOURS_TO_REMOVE * time.Hour)
	outdatedScore := outdatedDate.Format("20060102")
	_, err = j.Db.ZRemRangeByScore(monthlyKey, "0", outdatedScore).Result()
	if err != nil {
		j.Ack.Nack(j.Delivery, true)
		return FormatError(
			"Error while removing outdated keys from bucket %s: %#v",
			monthlyKey, err)
	}

	j.Ack.Ack(j.Delivery)
	return nil
}


func loopScheduleAggregation(client *redis.Client, ticker *time.Ticker, qconf Options) {
	conn, err := amqp.Dial(qconf.GetString("connection"))
	defer conn.Close()
	if err != nil {
		log.Panicf("Failed to connect to RabbitMQ: %#v", err)
	}

	for t := range ticker.C {
		log.Debugf("Tick: %#v", t)
		scheduleAggregation(client, uuid.New(), conn)
	}
}


func scheduleAggregation(client *redis.Client, appId string, conn *amqp.Connection) {
	now := time.Now().UTC()

	if !canExecute(client, appId, now) {
		return
	}

	res, err := client.SMembers("metrics").Result()
	if err != nil {
		log.Errorf("Can't retrive metrics keys: %#v", err)
		return
	}

	ch, err := conn.Channel()
	if err != nil {
		log.Errorf("Failed to open a channel: %#v", err)
		return
	}
	defer ch.Close()

	for _, key := range res {
		message, _ := json.Marshal(AggregateMsg{
			Metric:	key})

		err = ch.Publish(
			data.DistinctNameAggregateExchange.Name,
			data.DistinctNameAggregateExchange.DefaultRk,
			false,
			false,
			amqp.Publishing{
				ContentType:	"application/json",
				DeliveryMode:	amqp.Persistent,
				Body:			message})
	}
	updateExecuted(client, now)
}


func CalculateScore(msg Message) float64 {
	score, _ := strconv.ParseFloat(msg.Accepted.Format("20060102"), 64)
	return score
}


func canExecute(client *redis.Client, appId string, now time.Time) bool {
	val, err := client.Get("lastExecuted").Result()
	if err != nil {
		log.Errorf("Can't check if task already executed or not: %#v", err)
		return false
	}

	if val == now.Format("2006-01-02") {
		log.Debug("Skipped by %s as already executed")
		return false
	}

	ok, err := client.SetNX("lockedBy", appId, 1 * time.Hour).Result()
	if err != nil {
		log.Errorf("Can't tacke a lock: %#v", err)
		return false
	}

	if !ok {
		log.Debug("Failed to get lock")
		return false
	}
	return true
}


func updateExecuted(client *redis.Client, now time.Time) {
	err := client.Set("lastExecuted", now, 0).Err()
	if err != nil {
		log.Errorf("Failed to update lastExecuted: %#v", err)
	}
}
