package worker_test

import (
	"time"

	"github.com/streadway/amqp"
)


type MockAcknowledger struct {
	Acked bool
	Nacked bool
	Requeed bool
}


func (a *MockAcknowledger) Ack(d amqp.Delivery) {
	a.Acked = true
}


func (a *MockAcknowledger) Nack(d amqp.Delivery, requeue bool) {
	a.Nacked = true
	a.Requeed = requeue
}


func CompareDt(one time.Time, two time.Time) bool {
	if one == *new(time.Time) || two == *new(time.Time) {
		return false
	}
	one, two = one.Round(time.Millisecond), two.Round(time.Millisecond)
	return one.Equal(two)
}


func UTC() time.Time {
	return time.Now().UTC()
}

