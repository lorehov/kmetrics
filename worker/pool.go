package worker


type Job interface {
	Run() error
}


type Pool struct {
	ExecutorPool chan chan Job
	JobQueue chan Job
	ErrChan chan bool
	maxExecutors int
	executors []Executor
}


func NewPool(maxExecutors int) *Pool {
	return &Pool{
		ExecutorPool: make(chan chan Job, maxExecutors),
		JobQueue: make(chan Job),
		ErrChan: make(chan bool),
		executors: []Executor{},
		maxExecutors: maxExecutors}
}


func (s *Pool) Start() {
	for i :=0; i<=s.maxExecutors; i++ {
		executor := NewExecutor(s.ExecutorPool, s.ErrChan)
		s.executors = append(s.executors, executor)
		executor.Start()
	}

	go func() {
		s.dispatch()
	}()
}


func (s *Pool) Stop() {
	for _, e := range s.executors {
		e.Stop()
	}
}


func (s *Pool) dispatch() {
	for {
		select {
		case job := <- s.JobQueue:
			go func(job Job) {
				jobChannel := <-s.ExecutorPool
				jobChannel <- job
			}(job)
		}
	}
}


type Executor struct {
	ExecutorPool chan chan Job
	JobChannel chan Job
	quit chan bool
	err chan bool
}


func NewExecutor(pool chan chan Job, err chan bool) Executor {
	return Executor{
		ExecutorPool: pool,
		JobChannel: make(chan Job),
		quit: make(chan bool),
		err: err}
}


func (e Executor) Start() {
	go func() {
		for {
			e.ExecutorPool <- e.JobChannel
			select {
			case job := <-e.JobChannel:
				if err := job.Run(); err != nil {
					e.err <- true
				}
			case <-e.quit:
				return
			}
		}
	}()
}


func (e Executor) Stop() {
	e.quit <- true
}

