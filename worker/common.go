package worker
import (
	"fmt"
	"time"
	"errors"

	"github.com/streadway/amqp"
	log "github.com/Sirupsen/logrus"

	"bitbucket.org/lorehov/kmetrics/data"
	"github.com/spf13/cast"
)


const MAX_RETIRES_COUNT = 30


type Message struct {
	Username string		`json:"username", bson:"username"`
	Count int64			`json:"count", bson:"count"`
	Metric string		`json:"metric", bson:"metric"`
	Rid string			`json:"rid", bson:"rid"`
	Accepted time.Time	`json:"accepted", bson:"accepted"`
}


type Worker interface {
	Consume(pool *Pool, messages <-chan amqp.Delivery)
	Name() string
}


func RunWorker(qconf Options, exc data.Exchange, c int, worker Worker) error {
	conn, err := amqp.Dial(qconf.GetString("connection"))
	if err != nil {
		return FormatError("Failed to connect to RabbitMQ: %#v", err)
	}
	defer conn.Close()

	data.DeclareTopology(conn)

	closedListener := make(chan *amqp.Error)
	conn.NotifyClose(closedListener)
	ch, err := conn.Channel()
	if err != nil {
		return FormatError("Failed to open a channel: %#v", err)
	}
	defer ch.Close()

	q, err := ch.QueueDeclare(
		worker.Name(),
		false,
		false,
		true,
		false,
		amqp.Table{
			"x-dead-letter-exchange": data.DeadLettersExchange.Name,
			"x-dead-letter-routing-key": exc.DefaultRk})

	if err != nil {
		return FormatError("Failed to declare accountName queue: %#v", err)
	}

	err = ch.QueueBind(q.Name, exc.DefaultRk, exc.Name, false, nil)
	if err != nil {
		return FormatError("Failed to bind accountName queue to exchange: %#v", err)
	}

	err = ch.Qos(c, 0, false)
	if err != nil {
		return FormatError("Failed to setup Qos: %#v", err)
	}

	pool := NewPool(c)
	pool.Start()
	msgs, err := ch.Consume(q.Name, "", false, false, false, false, nil)
	log.Infof("Worker %s waiting for messages to process ...", worker.Name())
	go worker.Consume(pool, msgs)
	select {
	case <- pool.ErrChan:
		if ch != nil {
			// we must return all messages back to the broker
			ch.Close()
		}
		return FormatError("Unrecoverable error while processing task")
	case err := <- closedListener:
		return FormatError("Connection with rabbitmq lost: %#v", err)
	}
	return nil
}


type Acknowledger interface {
	Ack (d amqp.Delivery)
	Nack (d amqp.Delivery, requeue bool)
}


type AmqpAcknowledger struct {}


func (a *AmqpAcknowledger) Ack(d amqp.Delivery) {
	err := d.Ack(false)
	if err != nil {
		log.Errorf("Failed to ack delivery for message %q! %q", d.Body, err)
	}
}


func nack(d amqp.Delivery, r bool) {
	err := d.Nack(false, r)
	if err != nil {
		log.Errorf("Failed to nack delivery for message %q! %q", d.Body, err)
	}
}

func (a *AmqpAcknowledger) Nack(d amqp.Delivery, requeue bool) {
	if d.Redelivered {
		nack(d, false)
		return
	}
	nack(d, requeue)
}


func FormatError(str string, args ...interface{}) error {
	msg := fmt.Sprintf(str, args...)
	return errors.New(msg)
}


func Wait(c int) {
	time.Sleep(time.Duration(c * 2) * time.Second)
	c += 1
	if c == MAX_RETIRES_COUNT {
		log.Panicf("Max retries count")
	}
}


type Options map[string]interface{}


func (o Options) GetMap(key string) Options {
	return cast.ToStringMap(o[key])
}


func (o Options) GetInt(key string) int {
	return cast.ToInt(o[key])
}


func (o Options) GetInt64(key string) int64 {
	return int64(cast.ToInt(o[key]))
}


func (o Options) GetString(key string) string {
	return cast.ToString(o[key])
}
