package worker_test

import (
	"time"
	"testing"
	"math/rand"
	"encoding/json"

	"gopkg.in/mgo.v2"
	"github.com/streadway/amqp"
	"code.google.com/p/go-uuid/uuid"
	"github.com/spf13/viper"
	log "github.com/Sirupsen/logrus"
	"gopkg.in/mgo.v2/bson"

	"bitbucket.org/lorehov/kmetrics/worker"
	k "bitbucket.org/lorehov/kmetrics"

)


type testCaseH struct {
	Msgs []worker.Message
	StoredRids []string
}


func newTestCase(msgs []worker.Message, mask []bool) testCaseH {
	result := testCaseH{Msgs: msgs}
	storedRids := []string{}
	for i, _ := range msgs {
		rid := uuid.New()
		result.Msgs[i].Rid = rid
		result.Msgs[i].Username = string(rand.Intn(1024))
		if mask[i] {
			storedRids = append(storedRids, rid)
		}
	}
	result.StoredRids = storedRids
	return result
}


func isRidStored(rid string, c *mgo.Collection) bool {
	res, err := c.Find(bson.M{"rid": rid}).Count()
	if err != nil {
		log.Fatalf("Query to mongodb failed, %#v", err)
	}
	return res != 0
}


func TestHourlyLogWorker(t *testing.T) {
	k.InitConfig("config", true)
	session, err := mgo.Dial(viper.GetString("worker-hourly-log.mongodb.connection"))
	defer session.Close()
	if err != nil {
		log.Panicf("Couldn't connect to mongodb: %#v", err)
	}
	db := session.DB(viper.GetString("worker-hourly-log.mongodb.dbname-test"))

	metrics := db.C("metrics")
	averages := db.C("averages")

	current, acceptable, tooOld := UTC(), UTC().Add(-10 * time.Minute), UTC().Add(-2 * time.Hour)
	testCases := []testCaseH{
		newTestCase([]worker.Message{
			worker.Message{Metric: "la", Count: 10, Accepted: current}},
			[]bool{true}),
		newTestCase(
			[]worker.Message{
				worker.Message{Metric: "la", Count: 10, Accepted: current},
				worker.Message{Metric: "la", Count: 20, Accepted: current},
				worker.Message{Metric: "vss", Count: 100, Accepted: acceptable},
				worker.Message{Metric: "vss", Count: 200, Accepted: tooOld},
				worker.Message{Metric: "la", Count: 10, Accepted: tooOld}},
			[]bool{true, true, true, false, false}),
		newTestCase([]worker.Message{
			worker.Message{Metric: "la", Count: 10, Accepted: tooOld}},
			[]bool{false}),
	}

	for _, tc := range testCases {
		metrics.Remove(bson.M{})
		averages.Remove(bson.M{})
		for _, msg := range tc.Msgs {
			body, _ := json.Marshal(msg)
			job := worker.HourlyLogJob{
				Db: db,
				Ack: &MockAcknowledger{},
				Delivery: amqp.Delivery{
					Body: body,
				}}
			job.Run()
		}

		for _, r := range tc.StoredRids {
			if !isRidStored(r, metrics) {
				t.Errorf("Rid %#v must be stored tc: %#v", r, tc)
			}
		}
	}
}
